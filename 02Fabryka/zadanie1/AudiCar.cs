﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zadanie1
{
    public  class AudiCar : Car
    {
        public AudiCar()
        {
            name = "Audi a3";
            color = "red";
            type = "sedan";
            brand = "audi";
        }
    }
}
