﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zadanie1
{
    public class BmwCar : Car
    {
        public BmwCar()
        {
            name = "Bmw Z3";
            color = "green";
            type = "combi";
            brand = "bmw";
        }
    }
}
