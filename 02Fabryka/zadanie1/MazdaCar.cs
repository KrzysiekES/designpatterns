﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zadanie1
{
    public class MazdaCar : Car
    {
        public MazdaCar()
        {
            name = "Mazda 6";
            color = "green";
            type = "combi";
            brand = "mazda";
        }
    }
}
