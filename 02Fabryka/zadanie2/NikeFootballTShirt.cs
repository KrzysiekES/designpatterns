﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zadanie2
{
    class NikeFootballTShirt : TShirt
    {
        public NikeFootballTShirt()
        {
            name = "Real Madrid";
            type = "Normal";
            color = "white";
            size = "S";
        }
    }
}
