﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zadanie2
{
    class NikeSportTShirt : TShirt
    {
        public NikeSportTShirt()
        {
            name = "Nike Sport Razor";
            type = "Top";
            color = "orange";
            size = "XL";
        }
    }
}
