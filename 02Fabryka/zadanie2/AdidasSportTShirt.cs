﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zadanie2
{
    public class AdidasSportTShirt : TShirt
    {
        public AdidasSportTShirt()
        {
            name = "Adidas Mercurial";
            type = "Polo";
            color = "green";
            size = "XXL";
        }
    }
}
