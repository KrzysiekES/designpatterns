﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zadanie2
{
    class PumaFootballTShirt : TShirt
    {
        public PumaFootballTShirt()
        {
            name = "Puma Football Borussia Dortmund";
            type = "Normal";
            color = "yellow";
            size = "M";
        }
    }
}
