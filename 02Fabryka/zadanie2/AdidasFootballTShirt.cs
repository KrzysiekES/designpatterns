﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zadanie2
{
    class AdidasFootballTShirt : TShirt
    {
        public AdidasFootballTShirt()
        {
            name = "FC Barcelona Home kit";
            type = "Normal";
            color = "purple";
            size = "XS";
        }
    }
}
