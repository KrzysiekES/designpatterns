﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zadanie2
{
    class PumaSportTShirt : TShirt
    {
        public PumaSportTShirt()
        {
            name = "Puma Sport 2000";
            type = "Polo";
            color = "blue";
            size = "L";
        }
    }
}
